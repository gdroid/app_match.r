# what is it?

**app_match.R** is an unpolished R-script which matches f-droid apps by similarity of their descriptions.

Output is **a)** a list of all f-droid apps with their respective 'closest' matches and
**b)** a heatmap diagram for *each* of the apps.

app_match.R has been mentioned here  
https://forum.f-droid.org/t/app-match-r-find-similar-apps-on-f-droid-by-similarity-of-their-descriptions/4844

# what's the status?

"works for me..."

One currently has to:

* download and unpack [index-v1.jar](https://f-droid.org/repo/index-v1.jar)
* install.packages() within [R](https://www.r-project.org/) (I'm using [RStudio](https://en.wikipedia.org/wiki/RStudio) as IDE)
* eventually adapt paths (for the fdroiddata directory and index-v1.json)
* run the script interactively or on the command line "R -f app_match.R" or "./app_match.R --help"
* look at the \*.txt (and \*.out) files (when using 32 nearest neighbours these text files can be compacted to about 48 Byte (!) per app)
* look at the \*.png files (use about 100 MByte altogether)

**app_match.R** is employed in the fork https://gitlab.com/gdroid/app_match.r.  
It uses the **CI platform** to build up-to-date versions of **(a)** app_match.txt and app_match.out there. (It might use a slightly different or older version of **app_match.R**)

# plans?

* switch from fdroiddata/metadata to index-v1.jar (done)
* improve algorithm
* adapt the script so it can eventually be part of the f-droid infrastructure

# examples

Examples of the plots **(b)** that can be generated:  
<img src="sample-out/am_heatmap_de.k3b.android.androFotoFinder.1.png" width="240" height="240" />
focus on the terms that the matching is based on  
<img src="sample-out/am_heatmap_de.k3b.android.androFotoFinder.2.png" width="240" height="240" />
focus on description  
<img src="sample-out/am_heatmap_de.k3b.android.androFotoFinder.3.png" width="360" height="240" />
focus on Antifeatures and Android.Permissions
