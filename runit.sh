#!/bin/bash
rm -f app_match.out app_match.txt index-v1.jar index-v1.json ./archive-index-v1.json
#rm -rf fdroiddata

curl https://f-droid.org/archive/index-v1.jar > ./archive-index-v1.jar
unzip -j "./index-v1.jar" "index-v1.json" -d "./"
mv ./index-v1.json ./archive-index-v1.json

curl https://f-droid.org/repo/index-v1.jar > ./index-v1.jar
rm -f ./index-v1.json
unzip -j "./index-v1.jar" "index-v1.json" -d "./"

R -f install.R
R -f app_match.R

